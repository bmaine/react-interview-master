import React, { Component } from 'react';
import './App.css';
import Movies from './movies'
import MovieCard from './components/MovieCard';
import FilterByCategory from './components/FilterByCategory'



class App extends Component {
  constructor(props) {
    super(props);
    this.onDeleteCard = this.onDeleteCard.bind(this);
    this.onHandleLikes = this.onHandleLikes.bind(this);
    this.onSelectCategory = this.onSelectCategory.bind(this);

  }
  state = { currentMovies: Movies, currentPage: null, totalPages: null, numberOfCards: null, minCard: null, maxCard: null, numberOfCardsDisplayed:3, showCard: true, liked: null, categoryFilter: "All" };

  componentWillMount() {
    let categories = this.state.currentMovies.map(movie => movie.category);
    categories = Array.from(new Set(categories));
    this.setState({categories: categories})
  }

  onDeleteCard(data) {
    console.log('Card Deleted');
    const newCurrentMovies = this.state.currentMovies.filter(el => el.id !== data.id);
    this.setState({ currentMovies: newCurrentMovies });
  }

  onHandleLikes(event) {
    //console.log(this.state.currentMovies[event.target.id - 1].likes)
    let newCurrentMovies = this.state.currentMovies;
    (event.target.checked ? newCurrentMovies[event.target.id - 1].likes ++ : newCurrentMovies[event.target.id - 1].likes --);
    this.setState({ currentMovies: newCurrentMovies });
    console.log(this.state.currentMovies)
  }

  onSelectCategory(value) {
    this.setState({currentMovies: Movies});
    let newCurrentMovies =  [];
    console.log(value);
    this.setState({categoryFilter: value});
    this.state.currentMovies.forEach((item) => {
      if(item.category === value ) {
        newCurrentMovies.push(item)
      }
      this.setState({currentMovies: newCurrentMovies})

    });
    console.log(this.state.currentMovies)
  }

  render() {
    return (
      <div className="App">
        <header>

          <FilterByCategory categories={this.state.categories} selectCategory={this.onSelectCategory} />
        </header>
        <div className="w3-container">
          {this.state.currentMovies.map(movie => <MovieCard key={movie.id} card={this.state.currentMovies} title={movie.title} id={movie.id} likes={movie.likes} dislikes={movie.dislikes} category={movie.category} onClickDeleteCard={this.onDeleteCard} onHandleLikes={this.onHandleLikes} />) }
        </div>

      </div>

    );
  }
}

export default App;
