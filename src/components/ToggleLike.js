import React, { Component } from 'react';
import Toggle from 'react-toggle'
import "react-toggle/style.css"



class ToggleLike extends Component {
    constructor(props) {
        super(props)
        this.toogleVote = this.toogleVote.bind(this)

    }

    toogleVote(event) {
        this.props.onToggleLikeChange(event)
    }

    render() {

        return(
            <label>
                <Toggle className="Toggle-Like-Button"
                    onChange = {this.toogleVote}
                    id = {this.props.id}

                />
                <div className="Toggle-Like-Text">Votez içi</div>
            </label>
        )
    }
}

export default ToggleLike
