import React, { Component } from 'react';
import ToggleLike from './ToggleLike'



class MovieCard extends Component {
    constructor(props) {
        super(props)
        this.deleteButton = this.deleteButton.bind(this)
        this.toggleLikeChange = this.toggleLikeChange.bind(this)
    }


    deleteButton() {
        this.props.onClickDeleteCard(this.props)
    }

    toggleLikeChange(event) {
        event.preventDefault();
        this.props.onHandleLikes(event)
    }


    render() {

        let likesWeight = `${this.props.likes*95/(this.props.likes+this.props.dislikes)}%`;
        let disLikesWeight = `${this.props.dislikes*95/(this.props.likes+this.props.dislikes)}%`;

        return(
            <div className="Movie-card">
                <div style={{display:"block", width:"40vh"}}>
                    <div className="Movie-card-title">{this.props.title}</div>
                    <div>{this.props.category}</div>
                    <div className="Likes" style={{width: likesWeight}}></div>
                    <div className="Dislikes" style={{width: disLikesWeight}}></div>
                    <button className="Delete-Movie-Button" onClick={this.deleteButton}> Supprimer le film </button>
                    <ToggleLike id={this.props.id} onToggleLikeChange={this.toggleLikeChange}/>
                </div>
                <div>

                </div>


            </div>

        )
    }

}

export default MovieCard
