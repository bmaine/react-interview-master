import React, {Component} from 'react'

class FilterByCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {value: 'All'};

        this.handleChangeCategory = this.handleChangeCategory.bind(this);
        this.handleSubmitCategory = this.handleSubmitCategory.bind(this);
    }

    handleChangeCategory(event) {
        this.setState({value: event.target.value});
    }

    handleSubmitCategory(event) {
        alert('Your favorite flavor is: ' + this.state.value);
        console.log(event)
        event.preventDefault();
        this.props.selectCategory(this.state.value)
    }

    render() {
        return (
            <form onSubmit={this.handleSubmitCategory}>
                <label>
                    Choisissez unn type de film
                    <select value={this.state.value} onChange={this.handleChangeCategory}>
                        <option value="All">Toutes</option>
                        {this.props.categories.map(category => <option key={category} value={category}>{category}</option>)}
                    </select>
                </label>
                <input type="submit" value="Submit" />
            </form>
        );
    }
}

export default FilterByCategory